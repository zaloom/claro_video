import React, { Component } from 'react';
import $ from 'jquery';
import store from '../../store';
import './Home.css';

class Buscador extends Component {
    render() {
        return (
            <label>
            Buscar:&nbsp;
            <input className="Buscador-campo" type="text" name="buscar" id="buscar" />
            </label>
        );
    }
}

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '',
            total:''
        }

        store.subscribe(() => {
           this.setState({data: store.getState().data})
        });
    }

    componentWillMount() {
        const serviceURL = 'https://mfwkweb-api.clarovideo.net/services/content/list?api_version=v5.5&authpn=webclient&authpt=tfg1h3j4k6fd7&format=json&region=mexico&device_id=web&device_category=web&device_model=web&device_type=web&device_manufacturer=generic&HKS=3r74mrgkuvt9n4fc6r43itmk41&quantity=40&order_way=DESC&order_id=200&level_id=GPS&from=0&node_id=9869';

        fetch(serviceURL)
          .then((response) => {
            return response.json()
          })
          .then((result) => {
            this.setState({data: JSON.parse(JSON.stringify(result.response.groups)),total:result.response.total});
          })
    }

    componentDidMount() {

        $(window).scroll(function (event) {
            $( ".botonx" ).trigger( "click" );
        });

        $("#buscar").on('keyup', function(){
        var value = $(this).val().toLowerCase();
            if (value != '') {
                 $('.card').hide();
                 $('.card[class*="'+value+'"]').show();
            }else{
                $('.card').show();
            }
        }).keyup();
    }

    render() {
        var arr = [];
        var jsonList;

        if (this.state.data !== '') {
            var json = this.state.data;
            var total = this.state.total;
            Object.keys(json).forEach(function(key) {
              arr.push(json[key]);
            });

            jsonList = arr.map(function(json){
                var filterClass = json.title.replace(/\s/g,"_").toLowerCase();
                                return <div className={filterClass+" card"}>
                                            <div>
                                                <a href={"/ficha/"+json.id} title={json.title}>
                                                    <img src={json.image_small} alt={json.title} className="img" />
                                                </a>
                                            </div>
                                        </div>;
                              });
        }

        return (
          <div className="Home">
            <Buscador/>
            <div className="row Home-content">
            <div className="col-xs-12 col-sm-11 card-container">{jsonList}</div>
            <div className="botonx" onClick={() => this.addMovies(json, total)}></div>
            </div>
          </div>
        );
    }

    addMovies(json, total) {
        store.dispatch({
            type: "ADD_MOVIES",
            json,
            total
        })
    }
}

export default Home;
