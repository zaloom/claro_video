import React, { Component } from 'react';
import './Ficha.css';

class Ficha extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: ''
        }
    }

    componentWillMount() {
        const serviceURL = 'https://mfwkweb-api.clarovideo.net/services/content/data?api_version=v5.5&authpn=webclient&authpt=tfg1h3j4k6fd7&format=json&region=mexico&device_id=web&device_category=web&device_model=web&device_type=web&device_manufacturer=generic&HKS=p99gsv1q6g4anqb60ldt415ko5&group_id='+this.props.match.params.MovieId;

        fetch(serviceURL)
          .then((response) => {
            return response.json()
          })
          .then((result) => {
            this.setState({data: JSON.parse(JSON.stringify(result.response.group.common))});
          })
    }

    render() {
        var arr = [];
        var arrgen = [];
        var dataCard;
        var stafList;
        var genreList;

        if (this.state.data !== '') {
            var json = this.state.data;
            const divStyle = {
                background: 'url(' + json.image_background + ') no-repeat center center fixed',
                width: '100vw',
                height: '100vh',
            };

            var role = json.extendedcommon.roles.role;

            Object.keys(role).forEach(function(key) {
              arr.push(role[key]);
            });

            stafList = arr.map(function(json){
                                var arrTalent = [];
                                var talentList;
                                var jsonTalent = json.talents.talent;

                                Object.keys(jsonTalent).forEach(function(key) {
                                    arrTalent.push(jsonTalent[key]);
                                });

                                talentList = arrTalent.map(function(json){
                                    return <span><ins>{json.fullname}</ins>, </span>;
                                });

                                return <span><b>{json.name}:</b> {talentList}<br /></span>;
                              });

            var genre = json.extendedcommon.genres.genre;

            Object.keys(genre).forEach(function(key) {
              arrgen.push(genre[key]);
            });

            genreList = arrgen.map(function(json){
                        return <span>{json.name} </span>;
            });
            var arrTime= json.extendedcommon.media.duration.split(':')
            dataCard =  <div style={divStyle}>
                            <section className="Ficha">
                                <div className="col-xs-12">
                                    <h1 className="col-xs-12">{json.title}</h1>
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <img src={json.image_small} alt={json.title} className="imgficha" />
                                    </div>
                                    <div className="contenidoFicha col-xs-12 col-sm-5">
                                        <p>{json.large_description}</p>
                                        <p>
                                        <ul className="list-inline">
                                            <li>
                                                <span>{json.extendedcommon.media.publishyear}</span>
                                            </li>
                                            <li>
                                                <span className="padding-right-1">{arrTime[0]}h  {arrTime[1]}min  {arrTime[2]}s</span>
                                            </li>
                                            <li>
                                                <span className={"label label-default "+json.extendedcommon.media.language.subbed}>Subtitulada</span>
                                            </li>
                                            <li>
                                                <span className={"label label-default "+json.extendedcommon.media.language.dubbed}>Doblada</span>
                                            </li>
                                            <li>
                                                <span className="label label-default item-rating">{json.extendedcommon.media.rating.code}</span>
                                            </li>
                                        </ul>

                                        </p>
                                        <div className="staf">{stafList}</div>
                                        <span className="genero"><b>Género: </b>{genreList}<br /></span>
                                        <span className="genero"><b>Título original: </b>{json.extendedcommon.media.originaltitle}  <br /><br />
                                        </span>

                                        <div className="col-lg-8">
                                            <ul className="list-inline list-float">
                                                <li>
                                                    <div>
                                                        <div className="item-btn-wrapper">
                                                            <div className="btn btn-default btn-circle btn-lg item-fanbutton" id="card-email">
                                                                <i className="fa fa-envelope"></i>
                                                            </div>
                                                        </div>
                                                        <div className="item-btn-wrapper">
                                                            <span className="padding-top-05">e-mail</span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div >
                                                        <div className="item-btn-wrapper">
                                                            <div className=" " id="card-fb-like">
                                                                <button type="button" className="btn btn-default btn-circle btn-lg">
                                                                    <i className="fa fa-facebook pull-left marginfb"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div className="item-btn-wrapper">
                                                            <span className="padding-top-05">Compartir</span>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>;

        }

        return(
        <div className="Ficha">
            {dataCard}
        </div>
        );
    }
}

export default Ficha;
