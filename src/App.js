import React, { Component } from 'react';
import PropTypes from 'prop-types';
import logo from './clarovideo-logo-sitio.svg';
import './App.css';

class Logo extends Component {
    render() {
        return (
            <img src={logo} className="Logo-logo" alt="logo" />
        );
    }
}

class App extends Component {
    static propTypes = {
        children: PropTypes.object.isRequired
    };

    render() {
        const { children } = this.props;


        return (
          <div className="App">
            <div className="App-header">
                <a href="/"><Logo/> </a>
            </div>
            {children}
          </div>
        );
    }


}

export default App;
