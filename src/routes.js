import React from 'react';
import { Route, Switch } from 'react-router-dom';

//components
import App from './App';
import Ficha from './components/Ficha';
import Home from './components/Home';

const AppRoutes = () =>
    <App>
      <Switch>
        <Route path="/ficha/:MovieId" component={Ficha}/>
        <Route path="/" component={Home}/>
      </Switch>
    </App>

export default AppRoutes;
